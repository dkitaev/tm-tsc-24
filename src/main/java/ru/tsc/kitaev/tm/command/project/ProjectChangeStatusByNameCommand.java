package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change status project by name...";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        @Nullable final Project project = serviceLocator.getProjectService().changeStatusByName(userId, name, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}

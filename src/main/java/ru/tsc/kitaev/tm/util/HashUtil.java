package ru.tsc.kitaev.tm.util;

import org.jetbrains.annotations.NotNull;

public interface HashUtil {

    @NotNull String SECRET = "123123";

    @NotNull Integer ITERATION = 54356;

    @NotNull
    static String salt(@NotNull final String value) {
        @NotNull String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + value + SECRET);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull final String value) {
        try {
            @NotNull java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (final byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (@NotNull final java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}

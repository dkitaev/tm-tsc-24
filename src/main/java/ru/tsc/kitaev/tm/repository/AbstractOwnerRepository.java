package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IOwnerRepository;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @NotNull
    public static Predicate<AbstractOwnerEntity> predicateByUserId (@NotNull final String userId) {
        return s -> userId.equals(s.getUserId());
    }

    @Nullable
    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        if (!userId.equals(entity.getUserId())) return null;
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        if (!userId.equals(entity.getUserId())) return;
            list.remove(entity);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<E> entity = findAll(userId);
        list.removeAll(entity);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return findAll(userId).stream()
                .filter(predicateById(id))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<E> entities = findAll(userId);
        return entities.get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Optional<E> entity = Optional.ofNullable(findById(userId,id));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(EntityNotFoundException::new);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<E> entity = Optional.of(findByIndex(userId,index));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findById(userId, id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, final int index) {
        findByIndex(userId, index);
        return true;
    }

    @NotNull
    @Override
    public Integer getSize(@NotNull final String userId) {
        return (int) list.stream()
                .filter(predicateByUserId(userId))
                .count();
    }

}
